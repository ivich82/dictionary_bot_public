"""
The module contains methods to get data from the DB divided by types of objects.
"""
import logging
from logging import Logger

from sqlalchemy import select, and_, not_

from sample import database
from sample.database import Word, Meaning, WordMeaning

_logger: Logger = logging.getLogger(__name__)


class WordRepository:
    """
    Provides ways to find known words.
    """
    @staticmethod
    def select_by_value(word: str) -> Word:
        """
        Performs search of a database record by a given russian word.

        :param word: the given russian word to find
        :return: word record from the database
        """
        _logger.info('select_by_value started for word: %s', word)
        stmt = select(Word).where(Word.value == word)
        return database.get_session().scalars(stmt).one()


class MeaningRepository:
    """
    Provides ways to find known meanings.
    """
    @staticmethod
    def select_by_value(meaning: str) -> Meaning:
        """
        Performs search of a database record by a given meaning of a russian word.

        :param meaning: the given meaning to find
        :return: meaning record from the database
        """
        _logger.info('select_by_value started for meaning: %s', meaning)
        stmt = select(Meaning).where(Meaning.value == meaning)
        return database.get_session().scalars(stmt).one()


class WordMeaningRepository:
    """
    Provides ways to find known word-meaning pairs.
    """
    @staticmethod
    def select_proposed_with_limit(limit: int) -> list[WordMeaning]:
        """
        Fetches certain number of proposed word-meaning pairs (not yet approved).

        :param limit: how many word-meaning pairs to fetch
        :return: list of word-meaning pair records
        """
        _logger.info('select_proposed_with_limit started for limit: %s', limit)
        stmt = select(WordMeaning).where(not_(WordMeaning.approved)).limit(limit)
        return database.get_session().scalars(stmt).fetchall()

    @staticmethod
    def select_all_approved_for_word(word: str) -> list[WordMeaning]:
        """
        Fetches approved word-meaning pairs of a given russian word.

        :param word: the given russian word
        :return: list of word-meaning pair records
        """
        _logger.info('select_all_approved_for_word started for word: %s', word)
        stmt = select(WordMeaning).join(WordMeaning.word).where(and_(WordMeaning.approved, Word.value == word))
        return database.get_session().scalars(stmt).fetchall()
