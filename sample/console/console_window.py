"""
This module contains the admin console window definition as well as its elements.
"""
import logging
from logging import Logger
from typing import Callable

from ttkbootstrap import Window, Frame, Button, Label, DARK, LIGHT, INVERSE, INFO
from ttkbootstrap.dialogs import Messagebox

from sample import _
from sample.database import WordMeaning
from sample.repository import WordMeaningRepository
from sample.services import ProposedMeaningService

_logger: Logger = logging.getLogger(__name__)


def _show_error_alert(f):
    def decorated_function(*args, **kwargs):
        try:
            f(*args, **kwargs)
        except Exception as e:
            _logger.error(e, exc_info=True)
            Messagebox.show_error(title=_('Ошибка'), message=_('Ошибка базы данных. Попробуйте позже'))

    return decorated_function


class AdminConsole(Window):
    """
    The admin console window definition.
    """
    def __init__(self):
        super().__init__(title=_('Бот-словарь. Консоль администратора'),
                         size=(1024, 768), resizable=(False, False), hdpi=False)
        self._records: int = 5
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1, minsize=80)
        self.rowconfigure(1, weight=999)
        self._refresh_button: Button = Button(self, text=_('Обновить'), command=self._fetch_data, bootstyle=DARK)
        self._refresh_button.grid_propagate(False)
        self._refresh_button.grid(column=0, row=0, columnspan=5, padx=5, pady=1, sticky='nswe')
        self._fetch_data()

    @_show_error_alert
    def _fetch_data(self) -> None:
        """
        Fetches word-meaning pair objects from the database and constructs the table of it.
        """
        _logger.info('_fetch_data started')
        data: list[WordMeaning] = WordMeaningRepository.select_proposed_with_limit(self._records)
        self._table: Table = Table(self, data=data, fetch_command=self._fetch_data)
        self._table.grid(column=0, row=1, columnspan=5, padx=5, sticky='nswe')

    @property
    def records(self) -> int:
        """
        Number of word-meaning pair records displayed in the admin console window.
        """
        return self._records


class Table(Frame):
    """
    Table of the word-meaning pair objects in the admin console window.

    :param container: the admin console window
    :param data: the list of word-meaning pair objects from the database
    :param fetch_command: function which fetches word-meaning pair objects from the database
        and constructs the table of it
    """
    def __init__(self, container: AdminConsole, *, data: list[WordMeaning], fetch_command: Callable):
        super().__init__(container)
        self._offset: int = 0
        self._data: list[WordMeaning] = data
        self._table_rows: list[TableRow] = []
        self.columnconfigure(0, weight=1)
        for i in range(container.records):
            self.rowconfigure(i, weight=1, uniform='row')
        for i, proposed_meaning in enumerate(data):
            self.rowconfigure(i, weight=1, uniform='row')
            table_row: TableRow = TableRow(self, word_meaning=proposed_meaning,
                                           bootstyle=LIGHT, fetch_command=fetch_command)
            table_row.grid(column=0, row=i, pady=1, sticky='nswe')
            self._table_rows.append(table_row)


class TableRow(Frame):
    """
    A row of the word-meaning pair table representing a single word-meaning pair object.

    :param container: the table of the word-meaning pair objects
    :param word_meaning: the word-meaning pair object represented by this row
    :param fetch_command: function which fetches word-meaning pair objects from the database
        and constructs the table of it
    :param kwargs: arbitrary frame arguments passed to the parent constructor
    """
    def __init__(self, container: Table, *, word_meaning: WordMeaning, fetch_command: Callable, **kwargs):
        super().__init__(container, **kwargs)
        self._word_meaning: WordMeaning = word_meaning
        self._fetch_command: Callable = fetch_command

        self._word_meaning_frame = Frame(self)
        self._word_meaning_frame.rowconfigure(0, weight=1)
        self._word_meaning_frame.rowconfigure(1, weight=9)
        self._word_meaning_frame.columnconfigure(0, weight=1)
        self._word_label: Label = Label(self._word_meaning_frame,
                                        text=word_meaning.word.value, bootstyle=(INVERSE, INFO))
        self._meaning_label: Label \
            = Label(self._word_meaning_frame,
                    text=f'{word_meaning.meaning.value}\n\n{_("Предложил: {0}").format(word_meaning.proposed_by)}',
                    wraplength=700, justify='left', bootstyle=(INVERSE, LIGHT))
        self._word_label.grid(column=0, row=0, sticky='nwse')
        self._meaning_label.grid(column=0, row=1, sticky='nwse')

        self.columnconfigure(0, weight=1, minsize=728)
        self.columnconfigure(1, weight=1, minsize=24)
        self.columnconfigure(2, weight=1, minsize=20)
        self.rowconfigure(0, weight=1)
        self._accept_button: Button = Button(self, text=_('Добавить'), command=self._approve, bootstyle=DARK)
        self._reject_button: Button = Button(self, text=_('Удалить'), command=self._reject, bootstyle=DARK)
        self._word_meaning_frame.grid(column=0, row=0, sticky='nwse')
        self._accept_button.grid(column=1, row=0, sticky='nswe', padx=1)
        self._reject_button.grid(column=2, row=0, sticky='nswe')

    @_show_error_alert
    def _approve(self) -> None:
        """
        Calls the proposed meaning service to approve the word-meaning pair object and re-fetches the entire list.
        """
        _logger.info('_approve started for word meaning object: %s', self._word_meaning)
        ProposedMeaningService.approve_meaning(self._word_meaning)
        self._fetch_command()

    @_show_error_alert
    def _reject(self) -> None:
        """
        Calls the proposed meaning service to remove the word-meaning pair object and re-fetches the entire list.
        """
        _logger.info('_reject started for word meaning object: %s', self._word_meaning)
        ProposedMeaningService.delete_meaning(self._word_meaning)
        self._fetch_command()
