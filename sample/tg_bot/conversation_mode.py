from enum import Enum

CONVERSATION_MODE_KEY = 'mode'
"""Chat data key for conversation mode property."""
PROPOSE_MODE_WORD_KEY = 'word'
"""Chat data key for the current word property in the propose mode."""


class ConversationMode(Enum):
    """
    Set of conversation modes for a telegram bot chat.
    """
    RETRIEVE = 1
    """Telegram chat is in the retrieval mode - user gets approved word meanings."""
    PROPOSE = 2
    """Telegram chat is in the proposal mode - user sends proposed meaning for a word."""
