"""
This module contains the telegram bot application definition as well as its elements.
"""
import logging
import re
from logging import Logger

from sqlalchemy.exc import IntegrityError
from telegram import InlineKeyboardMarkup, InlineKeyboardButton, Update
from telegram.ext import Updater, CommandHandler, CallbackContext, MessageHandler, Filters, CallbackQueryHandler

from sample import _
from sample.secret import TOKEN
from sample.services import ProposedMeaningService
from sample.tg_bot.conversation_mode import ConversationMode, CONVERSATION_MODE_KEY, PROPOSE_MODE_WORD_KEY

_logger: Logger = logging.getLogger(__name__)


class DictionaryBotUpdater(Updater):
    """
    The telegram bot application definition.
    """
    _one_russian_word_pattern: str = r'^[А-Яа-яЁё]{1,32}$'
    _cancel_proposal_pattern: str = 'cancel'
    _say_hello_text: str = _('Привет! Я бот-словарь.')
    _invitation_text: str = _('Отправь мне одно слово из не более 32 русских букв, и я скажу, что оно означает.')
    _propose_invitation_template: str = _('Отправь мне своё значение слова "{0}". Максимум 256 символов')
    _cancel_button_markup: InlineKeyboardMarkup = InlineKeyboardMarkup([
        [InlineKeyboardButton(_('Отменить'), callback_data=_cancel_proposal_pattern)]
    ])
    _successful_proposal_text: str = _('Передал твоё предложение администраторам.')
    _duplicate_proposal_text: str = _('Это значение уже существует в словаре.')
    _error_text: str = _('Произошла ошибка. Попробуй позже.')

    @staticmethod
    def _build_markup(word: str) -> InlineKeyboardMarkup:
        """
        Provides the proposal mode button for a given russian word.

        :param word: the given russian word
        :return: button markup ready to be attached to a message
        """
        _logger.info('_build_markup started for word: %s', word)
        return InlineKeyboardMarkup([
            [InlineKeyboardButton(_('Предложить значение слова "{0}"').format(word), callback_data=word)]
        ])

    def __init__(self):
        super().__init__(TOKEN, use_context=True)
        self.dispatcher.add_handler(CommandHandler('start', self._start_command))
        self.dispatcher.add_handler(MessageHandler(filters=Filters.text, callback=self._message, pass_chat_data=True))
        self.dispatcher.add_handler(
            CallbackQueryHandler(pattern=self._one_russian_word_pattern, callback=self._enable_propose_mode))
        self.dispatcher.add_handler(
            CallbackQueryHandler(pattern=self._cancel_proposal_pattern, callback=self._disable_propose_mode))

    def _start_command(self, update: Update, context: CallbackContext) -> None:
        """
        Sends greetings to a user.

        :param update: incoming telegram bot update
        :param context: telegram bot handler context
        """
        _logger.info('_start_command started for chat id: %s', update.effective_chat.id)
        context.chat_data[CONVERSATION_MODE_KEY] = ConversationMode.RETRIEVE
        context.bot.send_message(
            text=self._say_hello_text,
            chat_id=update.effective_chat.id
        )
        context.bot.send_message(
            text=self._invitation_text,
            chat_id=update.effective_chat.id
        )

    def _message(self, update: Update, context: CallbackContext) -> None:
        """
        Processes incoming text messages.

        :param update: incoming telegram bot update
        :param context: telegram bot handler context
        """
        _logger.info('_message started for chat id: %s', update.effective_chat.id)
        if context.chat_data.get(CONVERSATION_MODE_KEY, ConversationMode.RETRIEVE) == ConversationMode.RETRIEVE \
                and re.match(self._one_russian_word_pattern, update.message.text):
            self._get_meaning(update.message.text, update, context)
        elif context.chat_data.get(CONVERSATION_MODE_KEY, ConversationMode.RETRIEVE) == ConversationMode.PROPOSE \
                and len(update.message.text) <= 256:
            self._propose_meaning(update.message.text, update, context)

    def _get_meaning(self, word: str, update: Update, context: CallbackContext) -> None:
        """
        Processes a given russian word meaning request.

        :param word: the given russian word
        :param update: incoming telegram bot update
        :param context: telegram bot handler context
        """
        _logger.info('_get_meaning started for chat id: %s', update.effective_chat.id)
        word = word.lower()
        try:
            context.bot.send_message(
                text=ProposedMeaningService.get_approved_meanings_reply(word),
                chat_id=update.effective_chat.id,
                reply_markup=self._build_markup(word)
            )
        except Exception as e:
            _logger.error(e, exc_info=True)
            context.bot.send_message(
                text=self._error_text,
                chat_id=update.effective_chat.id
            )

    def _enable_propose_mode(self, update: Update, context: CallbackContext) -> None:
        """
        Turns on the proposal mode when a user wants to propose a meaning.

        :param update: incoming telegram bot update
        :param context: telegram bot handler context
        """
        _logger.info('_enable_propose_mode started for chat id: %s', update.effective_chat.id)
        query = update.callback_query
        context.chat_data[CONVERSATION_MODE_KEY] = ConversationMode.PROPOSE
        context.chat_data[PROPOSE_MODE_WORD_KEY] = query.data
        query.answer()
        context.bot.send_message(
            text=self._propose_invitation_template.format(query.data),
            chat_id=update.effective_chat.id,
            reply_markup=self._cancel_button_markup
        )

    def _disable_propose_mode(self, update: Update, context: CallbackContext) -> None:
        """
        Turns off the proposal mode when a user wants to go back to normal operation.

        :param update: incoming telegram bot update
        :param context: telegram bot handler context
        """
        _logger.info('_disable_propose_mode started for chat id: %s', update.effective_chat.id)
        query = update.callback_query
        query.answer()
        if context.chat_data.get(CONVERSATION_MODE_KEY, ConversationMode.RETRIEVE) == ConversationMode.PROPOSE:
            context.chat_data[CONVERSATION_MODE_KEY] = ConversationMode.RETRIEVE
            context.bot.send_message(
                text=self._invitation_text,
                chat_id=update.effective_chat.id,
            )

    def _propose_meaning(self, meaning: str, update: Update, context: CallbackContext) -> None:
        """
        Processes a given meaning proposal request.

        :param meaning: the given meaning
        :param update: incoming telegram bot update
        :param context: telegram bot handler context
        :return:
        """
        _logger.info('_propose_meaning started for chat id: %s', update.effective_chat.id)
        meaning = meaning.lower()
        word: str = context.chat_data[PROPOSE_MODE_WORD_KEY]
        try:
            ProposedMeaningService.create_proposed_meaning(word=word, meaning=meaning,
                                                           proposed_by=update.effective_user.name)
            context.bot.send_message(
                text=self._successful_proposal_text,
                chat_id=update.effective_chat.id
            )
        except IntegrityError:
            _logger.info('Meaning %s of word %s is already in the database', meaning, word)
            context.bot.send_message(
                text=self._duplicate_proposal_text,
                chat_id=update.effective_chat.id
            )
        except Exception as e:
            _logger.error(e, exc_info=True)
            context.bot.send_message(
                text=self._error_text,
                chat_id=update.effective_chat.id
            )
        finally:
            context.chat_data[CONVERSATION_MODE_KEY] = ConversationMode.RETRIEVE
            context.bot.send_message(
                text=self._invitation_text,
                chat_id=update.effective_chat.id
            )
