from unittest import TestCase
from unittest.mock import patch

from sqlalchemy.exc import NoResultFound
from sqlalchemy.orm import Session

from sample import database
from sample.database import Word, Meaning, WordMeaning
from sample.repository import WordRepository, MeaningRepository, WordMeaningRepository
from test import drop_database, create_test_engine

_engine = create_test_engine()


@patch.object(database, 'get_session', return_value=Session(_engine))
class TestRepository(TestCase):
    maxDiff = None

    @classmethod
    def setUpClass(cls):
        with Session(_engine) as session:
            session.add_all([
                Word(value='тикток'),
                Word(value='обвал'),
                Word(value='растение'),
                Word(value='цветок'),
                Word(value='трава'),
                Meaning(value='социальная сеть для обмена короткими видеороликами'),
                Meaning(value='один видеоролик из этой же соцсети'),
                Meaning(value='см. обвалиться'),
                Meaning(value='снежные глыбы или обломки скал, обрушившиеся с гор'),
                Meaning(value='''организм, обычно развивающийся в неподвижном состоянии, получающий питание 
                (в отличие от животных) из воздуха (путем фотосинтеза) и почвы'''),
                Meaning(value='орган размножения растений с венчиком из лепестков вокруг пестика и тычинок'),
                Meaning(value='''травянистое растение, в пору цветения имеющее яркую, часто ароматную, 
                распускающуюся из бутона головку или соцветие'''),
                Meaning(value='яркая, часто ароматная головка или соцветие на стебле такого растения'),
                Meaning(value='''многолетнее или однолетнее растение с неодеревеневающим, 
                обычно мягким и зеленым невысоким стеблем'''),
                Meaning(value='зеленый покров земли из таких растений'),
                Meaning(value='травянистые растения, обладающие лечебными свойствами, входящие в лекарственные сборы'),
                Meaning(value='о чем-н. не имеющем вкуса, безвкусном (разг.)')
            ])
            session.commit()
            session.add_all([
                WordMeaning(word_id=1, meaning_id=1, proposed_by='@unittest', approved=True),
                WordMeaning(word_id=1, meaning_id=2, proposed_by='@testunit', approved=False),
                WordMeaning(word_id=2, meaning_id=3, proposed_by='@unittest', approved=True),
                WordMeaning(word_id=2, meaning_id=4, proposed_by='@testunit', approved=True),
                WordMeaning(word_id=3, meaning_id=5, proposed_by='@unittest', approved=True),
                WordMeaning(word_id=4, meaning_id=6, proposed_by='@testunit', approved=False),
                WordMeaning(word_id=4, meaning_id=7, proposed_by='@unittest', approved=False),
                WordMeaning(word_id=4, meaning_id=8, proposed_by='@testunit', approved=False),
                WordMeaning(word_id=5, meaning_id=9, proposed_by='@unittest', approved=False),
                WordMeaning(word_id=5, meaning_id=10, proposed_by='@testunit', approved=False),
                WordMeaning(word_id=5, meaning_id=11, proposed_by='@unittest', approved=False),
                WordMeaning(word_id=5, meaning_id=12, proposed_by='@testunit', approved=False)
            ])
            session.commit()

    @classmethod
    def tearDownClass(cls) -> None:
        drop_database(_engine)

    def test_select_word_by_value_existing(self, fake_session):
        word_object: Word = WordRepository.select_by_value('тикток')
        self.assertEqual(1, fake_session.call_count)
        self.assertEqual('тикток', word_object.value)

    def test_select_word_by_value_not_existing(self, fake_session):
        with self.assertRaises(NoResultFound):
            WordRepository.select_by_value('слово')
        self.assertEqual(1, fake_session.call_count)

    def test_select_meaning_by_value_existing(self, fake_session):
        meaning_object: Meaning \
            = MeaningRepository.select_by_value('социальная сеть для обмена короткими видеороликами')
        self.assertEqual(1, fake_session.call_count)
        self.assertEqual('социальная сеть для обмена короткими видеороликами', meaning_object.value)

    def test_select_meaning_by_value_not_existing(self, fake_session):
        with self.assertRaises(NoResultFound):
            MeaningRepository.select_by_value('значение')
        self.assertEqual(1, fake_session.call_count)

    def test_select_all_approved_for_word_not_existing(self, fake_session):
        word_meaning_objects: list[WordMeaning] = WordMeaningRepository.select_all_approved_for_word('слово')
        self.assertEqual(1, fake_session.call_count)
        self.assertEqual(0, len(word_meaning_objects))

    def test_select_all_approved_for_word_with_no_approved(self, fake_session):
        word_meaning_objects: list[WordMeaning] = WordMeaningRepository.select_all_approved_for_word('цветок')
        self.assertEqual(1, fake_session.call_count)
        self.assertEqual(0, len(word_meaning_objects))

    def test_select_all_approved_for_word_partially_approved(self, fake_session):
        word_meaning_objects: list[WordMeaning] = WordMeaningRepository.select_all_approved_for_word('тикток')
        self.assertEqual(1, fake_session.call_count)
        self.assertEqual(1, len(word_meaning_objects))
        self.assertEqual('социальная сеть для обмена короткими видеороликами', word_meaning_objects[0].meaning.value)

    def test_select_all_approved_for_word_all_approved(self, fake_session):
        word_meaning_objects: list[WordMeaning] = WordMeaningRepository.select_all_approved_for_word('обвал')
        self.assertEqual(1, fake_session.call_count)
        self.assertCountEqual(
            ['см. обвалиться', 'снежные глыбы или обломки скал, обрушившиеся с гор'],
            [obj.meaning.value for obj in word_meaning_objects])

    def test_select_proposed_with_over_limit(self, fake_session):
        word_meaning_objects: list[WordMeaning] = WordMeaningRepository.select_proposed_with_limit(10)
        self.assertEqual(1, fake_session.call_count)
        self.assertCountEqual(
            [
                'один видеоролик из этой же соцсети',
                'орган размножения растений с венчиком из лепестков вокруг пестика и тычинок',
                '''травянистое растение, в пору цветения имеющее яркую, часто ароматную, 
                распускающуюся из бутона головку или соцветие''',
                'яркая, часто ароматная головка или соцветие на стебле такого растения',
                '''многолетнее или однолетнее растение с неодеревеневающим, 
                обычно мягким и зеленым невысоким стеблем''',
                'зеленый покров земли из таких растений',
                'травянистые растения, обладающие лечебными свойствами, входящие в лекарственные сборы',
                'о чем-н. не имеющем вкуса, безвкусном (разг.)'
            ],
            [obj.meaning.value for obj in word_meaning_objects])

    def test_select_proposed_with_under_limit(self, fake_session):
        word_meaning_objects: list[WordMeaning] = WordMeaningRepository.select_proposed_with_limit(5)
        self.assertEqual(1, fake_session.call_count)
        actual_meanings: set[str] = {obj.meaning.value for obj in word_meaning_objects}
        self.assertEqual(
            actual_meanings & {
                'один видеоролик из этой же соцсети',
                'орган размножения растений с венчиком из лепестков вокруг пестика и тычинок',
                '''травянистое растение, в пору цветения имеющее яркую, часто ароматную, 
                распускающуюся из бутона головку или соцветие''',
                'яркая, часто ароматная головка или соцветие на стебле такого растения',
                '''многолетнее или однолетнее растение с неодеревеневающим, 
                обычно мягким и зеленым невысоким стеблем''',
                'зеленый покров земли из таких растений',
                'травянистые растения, обладающие лечебными свойствами, входящие в лекарственные сборы',
                'о чем-н. не имеющем вкуса, безвкусном (разг.)'},
            actual_meanings)
