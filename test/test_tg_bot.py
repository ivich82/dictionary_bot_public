import collections
import datetime
from unittest import TestCase
from unittest.mock import patch

from sqlalchemy.exc import IntegrityError
from telegram import Update, Bot, Chat, Message, InlineKeyboardMarkup, InlineKeyboardButton, User, CallbackQuery
from telegram.constants import CHAT_PRIVATE
from telegram.ext import CallbackContext

from sample import _
from sample.database import WordMeaning, Meaning
from sample.repository import WordMeaningRepository
from sample.services import ProposedMeaningService
from sample.tg_bot.conversation_mode import CONVERSATION_MODE_KEY, ConversationMode, PROPOSE_MODE_WORD_KEY
from sample.tg_bot.updater import DictionaryBotUpdater


class TestTgBot(TestCase):
    _users: list[User] = [User(id=1234567, first_name=_username, is_bot=False)
                          for _username in ['@unittest', '@testunit']]
    _fake_words: list[str] = ['tiktok', 'tiktokтикток', 'тиктоктиктоктиктоктиктоктиктоктикток', 'тикток тикток']
    _real_words: list[str] = ['тикток', 'обвал']
    _real_meanings: list[tuple[str, str, User]] = [
        ('тикток', 'соцсеть для obmena короткими видеороликами', _users[0]),
        ('тикток', 'Один видеоролик из этой соцсети !@#$%^&*()_+/\\\'"?<>[]{}|', _users[1]),
        ('обвал', 'см. обвалиться レオ・オーンスタイン'.encode('utf-8'), _users[1]),
        ('обвал', 'Cнежные глыбы \r\n или обломки \U0001F609скал\n, обрушившиеся \t\tс гор'.encode('utf-8'), _users[0])]

    _tg_bot: DictionaryBotUpdater = DictionaryBotUpdater()
    _chat_id: int = 123456789
    _update_id: int = 1234
    _message_id: int = 123456
    _query_id: int = 123

    _chat: Chat = Chat(id=_chat_id, type=CHAT_PRIVATE)
    _query = CallbackQuery(id=str(_query_id), from_user=_users[0], chat_instance=str(_chat_id))
    _update: Update = Update(update_id=_update_id, callback_query=_query)
    _update._effective_chat = _chat
    _context: CallbackContext = CallbackContext(dispatcher=_tg_bot.dispatcher)
    _context._chat_id_and_data = (_chat_id, {})

    @patch.object(Bot, 'send_message')
    def test_start_command(self, fake_send_message):
        self._tg_bot._start_command(self._update, self._context)
        self.assertEqual(2, fake_send_message.call_count)
        self.assertEqual(
            [
                ({'text': self._tg_bot._say_hello_text, 'chat_id': self._chat_id},),
                ({'text': self._tg_bot._invitation_text, 'chat_id': self._chat_id},)],
            fake_send_message.call_args_list)

    def test_word_not_matching_pattern_empty_chat_data(self):
        self._context._chat_id_and_data[1].clear()
        for fake_word in self._fake_words:
            with self.subTest(fake_word=fake_word), \
                    patch.object(DictionaryBotUpdater, '_get_meaning') as fake_get_meaning, \
                    patch.object(DictionaryBotUpdater, '_propose_meaning') as fake_propose_meaning:
                self._update.message = Message(message_id=self._message_id,
                                               date=datetime.datetime.utcnow(),
                                               chat=self._chat, text=fake_word)
                self._tg_bot._message(self._update, self._context)
                self.assertEqual(0, fake_get_meaning.call_count)
                self.assertEqual(0, fake_propose_meaning.call_count)

    def test_word_not_matching_pattern_retrieve_mode(self):
        self._context._chat_id_and_data[1][CONVERSATION_MODE_KEY] = ConversationMode.RETRIEVE
        for fake_word in self._fake_words:
            with self.subTest(fake_word=fake_word), \
                    patch.object(DictionaryBotUpdater, '_get_meaning') as fake_get_meaning, \
                    patch.object(DictionaryBotUpdater, '_propose_meaning') as fake_propose_meaning:
                self._update.message = Message(message_id=self._message_id,
                                               date=datetime.datetime.utcnow(),
                                               chat=self._chat, text=fake_word)
                self._tg_bot._message(self._update, self._context)
                self.assertEqual(0, fake_get_meaning.call_count)
                self.assertEqual(0, fake_propose_meaning.call_count)

    @patch.object(DictionaryBotUpdater, '_propose_meaning')
    @patch.object(DictionaryBotUpdater, '_get_meaning')
    def test_word_matching_pattern_empty_chat_data(self, fake_get_meaning, fake_propose_meaning):
        self._context._chat_id_and_data[1].clear()
        self._update.message = Message(message_id=self._message_id,
                                       date=datetime.datetime.utcnow(),
                                       chat=self._chat, text=self._real_words[0])
        self._tg_bot._message(self._update, self._context)
        self.assertEqual(1, fake_get_meaning.call_count)
        self.assertEqual(0, fake_propose_meaning.call_count)

    @patch.object(DictionaryBotUpdater, '_propose_meaning')
    @patch.object(DictionaryBotUpdater, '_get_meaning')
    def test_word_matching_pattern_retrieve_mode(self, fake_get_meaning, fake_propose_meaning):
        self._context._chat_id_and_data[1][CONVERSATION_MODE_KEY] = ConversationMode.RETRIEVE
        self._update.message = Message(message_id=self._message_id,
                                       date=datetime.datetime.utcnow(),
                                       chat=self._chat, text=self._real_words[1])
        self._tg_bot._message(self._update, self._context)
        self.assertEqual(1, fake_get_meaning.call_count)
        self.assertEqual(0, fake_propose_meaning.call_count)

    def test_get_meaning_nothing_found(self):
        for real_word in self._real_words + list(map(str.capitalize, self._real_words)):
            with self.subTest(real_word=real_word), \
                    patch.object(Bot, 'send_message') as fake_send_message, \
                    patch.object(WordMeaningRepository, 'select_all_approved_for_word', return_value=[]) \
                            as fake_select_all_approved_for_word:
                self._tg_bot._get_meaning(real_word, self._update, self._context)
                self.assertEqual(1, fake_select_all_approved_for_word.call_count)
                self.assertEqual(((real_word.lower(),),), fake_select_all_approved_for_word.call_args)
                self.assertEqual(1, fake_send_message.call_count)
                expected_markup: InlineKeyboardMarkup = InlineKeyboardMarkup([
                    [InlineKeyboardButton(
                        _('Предложить значение слова "{0}"').format(real_word.lower()),
                        callback_data=real_word.lower())]
                ])
                self.assertEqual(
                    [
                        ({'text': ProposedMeaningService._word_or_meaning_not_found_text,
                          'chat_id': self._chat_id,
                          'reply_markup': expected_markup},)],
                    fake_send_message.call_args_list)

    @patch.object(ProposedMeaningService, 'get_approved_meanings_reply',
                  return_value='тикток:\n\nсоциальная сеть для обмена короткими видеороликами')
    @patch.object(Bot, 'send_message')
    def test_get_meaning_success(self, fake_send_message, fake_get_approved_meanings_reply):
        real_word: str = 'тикток'
        self._tg_bot._get_meaning(real_word, self._update, self._context)
        self.assertEqual(1, fake_get_approved_meanings_reply.call_count)
        self.assertEqual(((real_word,),), fake_get_approved_meanings_reply.call_args)
        self.assertEqual(1, fake_send_message.call_count)
        expected_markup: InlineKeyboardMarkup = InlineKeyboardMarkup([
            [InlineKeyboardButton(
                _('Предложить значение слова "{0}"').format(real_word),
                callback_data=real_word)]
        ])
        self.assertEqual(
            [
                ({'text': 'тикток:\n\nсоциальная сеть для обмена короткими видеороликами',
                  'chat_id': self._chat_id,
                  'reply_markup': expected_markup},)],
            fake_send_message.call_args_list)

    @patch.object(ProposedMeaningService, 'get_approved_meanings_reply',
                  return_value='обвал:\n\n1. см. обвалиться\n2. снежные глыбы или обломки скал, обрушившиеся с гор')
    @patch.object(Bot, 'send_message')
    def test_get_meaning_success_capitalized(self, fake_send_message, fake_get_approved_meanings_reply):
        real_word: str = 'Обвал'
        self._tg_bot._get_meaning(real_word, self._update, self._context)
        self.assertEqual(1, fake_get_approved_meanings_reply.call_count)
        self.assertEqual(((real_word.lower(),),), fake_get_approved_meanings_reply.call_args)
        self.assertEqual(1, fake_send_message.call_count)
        expected_markup: InlineKeyboardMarkup = InlineKeyboardMarkup([
            [InlineKeyboardButton(
                _('Предложить значение слова "{0}"').format(real_word.lower()),
                callback_data=real_word.lower())]
        ])
        self.assertEqual(
            [
                ({'text': 'обвал:\n\n1. см. обвалиться\n2. снежные глыбы или обломки скал, обрушившиеся с гор',
                  'chat_id': self._chat_id,
                  'reply_markup': expected_markup},)],
            fake_send_message.call_args_list)

    @patch.object(ProposedMeaningService, 'get_approved_meanings_reply', side_effect=Exception())
    @patch.object(Bot, 'send_message')
    def test_get_meaning_error(self, fake_send_message, fake_get_approved_meanings_reply):
        real_word: str = 'тикток'
        self._tg_bot._get_meaning(real_word, self._update, self._context)
        self.assertEqual(1, fake_get_approved_meanings_reply.call_count)
        self.assertEqual(((real_word,),), fake_get_approved_meanings_reply.call_args)
        self.assertEqual(1, fake_send_message.call_count)
        self.assertEqual(
            [
                ({'text': self._tg_bot._error_text,
                  'chat_id': self._chat_id},)],
            fake_send_message.call_args_list)

    def test_enable_propose_mode_from_empty_chat_data(self):
        self._context._chat_id_and_data[1].clear()
        for real_word in self._real_words:
            with self.subTest(real_word=real_word), \
                    patch.object(Bot, 'send_message') as fake_send_message, \
                    patch.object(CallbackQuery, 'answer') as fake_answer:
                self._query.data = real_word
                self._tg_bot._enable_propose_mode(self._update, self._context)
                self.assertEqual(1, fake_answer.call_count)
                self.assertEqual(1, fake_send_message.call_count)
                expected_markup: InlineKeyboardMarkup = InlineKeyboardMarkup([
                    [InlineKeyboardButton(_('Отменить'), callback_data=self._tg_bot._cancel_proposal_pattern)]])
                self.assertEqual(
                    [
                        ({'text': self._tg_bot._propose_invitation_template.format(real_word),
                          'chat_id': self._chat_id,
                          'reply_markup': expected_markup},)],
                    fake_send_message.call_args_list)
                self.assertEqual(
                    {CONVERSATION_MODE_KEY: ConversationMode.PROPOSE, PROPOSE_MODE_WORD_KEY: real_word},
                    self._context.chat_data)

    def test_enable_propose_mode_from_retrieve_mode(self):
        self._context._chat_id_and_data[1][CONVERSATION_MODE_KEY] = ConversationMode.RETRIEVE
        for real_word in self._real_words:
            with self.subTest(real_word=real_word), \
                    patch.object(Bot, 'send_message') as fake_send_message, \
                    patch.object(CallbackQuery, 'answer') as fake_answer:
                self._query.data = real_word
                self._tg_bot._enable_propose_mode(self._update, self._context)
                self.assertEqual(1, fake_answer.call_count)
                self.assertEqual(1, fake_send_message.call_count)
                expected_markup: InlineKeyboardMarkup = InlineKeyboardMarkup([
                    [InlineKeyboardButton(_('Отменить'), callback_data=self._tg_bot._cancel_proposal_pattern)]])
                self.assertEqual(
                    [
                        ({'text': self._tg_bot._propose_invitation_template.format(real_word),
                          'chat_id': self._chat_id,
                          'reply_markup': expected_markup},)],
                    fake_send_message.call_args_list)
                self.assertEqual(
                    {CONVERSATION_MODE_KEY: ConversationMode.PROPOSE, PROPOSE_MODE_WORD_KEY: real_word},
                    self._context.chat_data)

    def test_enable_propose_mode_from_propose_mode_another_word(self):
        self._context._chat_id_and_data[1][CONVERSATION_MODE_KEY] = ConversationMode.PROPOSE
        next_words: collections.deque[str] = collections.deque(self._real_words)
        next_words.rotate(1)
        for real_word, next_word in zip(self._real_words, next_words):
            with self.subTest(real_word=real_word, next_word=next_word), \
                    patch.object(Bot, 'send_message') as fake_send_message, \
                    patch.object(CallbackQuery, 'answer') as fake_answer:
                self._context._chat_id_and_data[1][PROPOSE_MODE_WORD_KEY] = next_word
                self._query.data = real_word
                self._tg_bot._enable_propose_mode(self._update, self._context)
                self.assertEqual(1, fake_answer.call_count)
                self.assertEqual(1, fake_send_message.call_count)
                expected_markup: InlineKeyboardMarkup = InlineKeyboardMarkup([
                    [InlineKeyboardButton(_('Отменить'), callback_data=self._tg_bot._cancel_proposal_pattern)]])
                self.assertEqual(
                    [
                        ({'text': self._tg_bot._propose_invitation_template.format(real_word),
                          'chat_id': self._chat_id,
                          'reply_markup': expected_markup},)],
                    fake_send_message.call_args_list)
                self.assertEqual(
                    {CONVERSATION_MODE_KEY: ConversationMode.PROPOSE, PROPOSE_MODE_WORD_KEY: real_word},
                    self._context.chat_data)

    @patch.object(Bot, 'send_message')
    @patch.object(CallbackQuery, 'answer')
    def test_disable_propose_mode_from_empty_chat_data(self, fake_answer, fake_send_message):
        self._context._chat_id_and_data[1].clear()
        self._tg_bot._disable_propose_mode(self._update, self._context)
        self.assertEqual(1, fake_answer.call_count)
        self.assertEqual(0, fake_send_message.call_count)
        self.assertEqual({}, self._context.chat_data)

    @patch.object(Bot, 'send_message')
    @patch.object(CallbackQuery, 'answer')
    def test_disable_propose_mode_from_retrieve_mode(self, fake_answer, fake_send_message):
        self._context._chat_id_and_data[1][CONVERSATION_MODE_KEY] = ConversationMode.RETRIEVE
        self._tg_bot._disable_propose_mode(self._update, self._context)
        self.assertEqual(1, fake_answer.call_count)
        self.assertEqual(0, fake_send_message.call_count)
        self.assertEqual(ConversationMode.RETRIEVE, self._context.chat_data[CONVERSATION_MODE_KEY])

    def test_disable_propose_mode_from_propose_mode(self):
        for real_word in self._real_words:
            with self.subTest(real_word=real_word), \
                    patch.object(Bot, 'send_message') as fake_send_message, \
                    patch.object(CallbackQuery, 'answer') as fake_answer:
                self._context._chat_id_and_data[1][CONVERSATION_MODE_KEY] = ConversationMode.PROPOSE
                self._context._chat_id_and_data[1][PROPOSE_MODE_WORD_KEY] = real_word
                self._tg_bot._disable_propose_mode(self._update, self._context)
                self.assertEqual(1, fake_answer.call_count)
                self.assertEqual(1, fake_send_message.call_count)
                self.assertEqual(
                    [({'text': self._tg_bot._invitation_text, 'chat_id': self._chat_id},)],
                    fake_send_message.call_args_list)
                self.assertEqual(
                    {CONVERSATION_MODE_KEY: ConversationMode.RETRIEVE, PROPOSE_MODE_WORD_KEY: real_word},
                    self._context.chat_data)

    @patch.object(DictionaryBotUpdater, '_propose_meaning')
    @patch.object(DictionaryBotUpdater, '_get_meaning')
    def test_too_long_proposed_meaning(self, fake_get_meaning, fake_propose_meaning):
        self._context._chat_id_and_data[1][CONVERSATION_MODE_KEY] = ConversationMode.PROPOSE
        self._update.message = Message(message_id=self._message_id,
                                       date=datetime.datetime.utcnow(),
                                       chat=self._chat, text='ы' * 300)
        self._tg_bot._message(self._update, self._context)
        self.assertEqual(0, fake_get_meaning.call_count)
        self.assertEqual(0, fake_propose_meaning.call_count)

    @patch.object(DictionaryBotUpdater, '_propose_meaning')
    @patch.object(DictionaryBotUpdater, '_get_meaning')
    def test_valid_proposed_meaning(self, fake_get_meaning, fake_propose_meaning):
        self._context._chat_id_and_data[1][CONVERSATION_MODE_KEY] = ConversationMode.PROPOSE
        self._update.message = Message(message_id=self._message_id,
                                       date=datetime.datetime.utcnow(),
                                       chat=self._chat, text='ы' * 200)
        self._tg_bot._message(self._update, self._context)
        self.assertEqual(0, fake_get_meaning.call_count)
        self.assertEqual(1, fake_propose_meaning.call_count)

    def test_propose_meaning(self):
        for word, meaning, user in self._real_meanings:
            with self.subTest(word=word, meaning=meaning), \
                    patch.object(Bot, 'send_message') as fake_send_message, \
                    patch.object(ProposedMeaningService, 'create_proposed_meaning') as fake_create_proposed_meaning:
                self._context._chat_id_and_data[1][CONVERSATION_MODE_KEY] = ConversationMode.PROPOSE
                self._context._chat_id_and_data[1][PROPOSE_MODE_WORD_KEY] = word
                self._update._effective_user = user
                self._tg_bot._propose_meaning(meaning, self._update, self._context)
                self.assertEqual(1, fake_create_proposed_meaning.call_count)
                self.assertEqual(
                    ({'word': word, 'meaning': meaning.lower(), 'proposed_by': user.name}, ),
                    fake_create_proposed_meaning.call_args)
                self.assertEqual(2, fake_send_message.call_count)
                self.assertEqual(
                    [
                        ({'text': self._tg_bot._successful_proposal_text, 'chat_id': self._chat_id},),
                        ({'text': self._tg_bot._invitation_text, 'chat_id': self._chat_id},)],
                    fake_send_message.call_args_list)
                self.assertEqual(
                    {CONVERSATION_MODE_KEY: ConversationMode.RETRIEVE, PROPOSE_MODE_WORD_KEY: word},
                    self._context.chat_data)

    @patch.object(ProposedMeaningService, 'create_proposed_meaning',
                  side_effect=IntegrityError('stmt', 'params', 'orig'))
    @patch.object(Bot, 'send_message')
    def test_propose_meaning_integrity_error(self, fake_send_message, fake_create_proposed_meaning):
        word, meaning, user = self._real_meanings[0]
        self._context._chat_id_and_data[1][CONVERSATION_MODE_KEY] = ConversationMode.PROPOSE
        self._context._chat_id_and_data[1][PROPOSE_MODE_WORD_KEY] = word
        self._update._effective_user = user
        self._tg_bot._propose_meaning(meaning, self._update, self._context)
        self.assertEqual(1, fake_create_proposed_meaning.call_count)
        self.assertEqual(
            ({'word': word, 'meaning': meaning.lower(), 'proposed_by': user.name}, ),
            fake_create_proposed_meaning.call_args)
        self.assertEqual(2, fake_send_message.call_count)
        self.assertEqual(
            [
                ({'text': self._tg_bot._duplicate_proposal_text, 'chat_id': self._chat_id},),
                ({'text': self._tg_bot._invitation_text, 'chat_id': self._chat_id},)],
            fake_send_message.call_args_list)
        self.assertEqual(
            {CONVERSATION_MODE_KEY: ConversationMode.RETRIEVE, PROPOSE_MODE_WORD_KEY: word},
            self._context.chat_data)

    @patch.object(ProposedMeaningService, 'create_proposed_meaning', side_effect=Exception())
    @patch.object(Bot, 'send_message')
    def test_propose_meaning_generic_error(self, fake_send_message, fake_create_proposed_meaning):
        word, meaning, user = self._real_meanings[0]
        self._context._chat_id_and_data[1][CONVERSATION_MODE_KEY] = ConversationMode.PROPOSE
        self._context._chat_id_and_data[1][PROPOSE_MODE_WORD_KEY] = word
        self._update._effective_user = user
        self._tg_bot._propose_meaning(meaning, self._update, self._context)
        self.assertEqual(1, fake_create_proposed_meaning.call_count)
        self.assertEqual(
            ({'word': word, 'meaning': meaning.lower(), 'proposed_by': user.name}, ),
            fake_create_proposed_meaning.call_args)
        self.assertEqual(2, fake_send_message.call_count)
        self.assertEqual(
            [
                ({'text': self._tg_bot._error_text, 'chat_id': self._chat_id},),
                ({'text': self._tg_bot._invitation_text, 'chat_id': self._chat_id},)],
            fake_send_message.call_args_list)
        self.assertEqual(
            {CONVERSATION_MODE_KEY: ConversationMode.RETRIEVE, PROPOSE_MODE_WORD_KEY: word},
            self._context.chat_data)
